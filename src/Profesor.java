import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Profesor extends Persona{

	private int categoria;

	public int getCategoria() {
		return categoria;
	}

	public void setCategoria(int categoria) {
		this.categoria = categoria;
	}

	public Profesor(String dNI, String nombre, int categoria) {
		super(dNI, nombre);
		this.categoria = categoria;
	}

	public Profesor() {
		super();
	}
	
	@Override
	public String toString() {
		return "Profesor [categoria=" + categoria + ", DNI=" + DNI + ", nombre=" + nombre + "]";
	}

	
}
