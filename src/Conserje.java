import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Conserje extends Persona{

	private boolean diurno;

	public boolean isDiurno() {
		return diurno;
	}

	public void setDiurno(boolean diurno) {
		this.diurno = diurno;
	}

	public Conserje(String dNI, String nombre, boolean diurno) {
		super(dNI, nombre);
		this.diurno = diurno;
	}

	public Conserje() {
		super();
	}
	
	@Override
	public String toString() {
		return "Conserje [diurno=" + diurno + ", DNI=" + DNI + ", nombre=" + nombre + "]";
	}
	
}
