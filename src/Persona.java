
public class Persona {

	protected String DNI;
	protected String nombre;
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public Persona(String dNI, String nombre) {
		super();
		DNI = dNI;
		this.nombre = nombre;
	}
	public Persona() {
		super();
	}
	@Override
	public String toString() {
		return "Persona [DNI=" + DNI + ", nombre=" + nombre + "]";
	}
	
	
	
	
}
