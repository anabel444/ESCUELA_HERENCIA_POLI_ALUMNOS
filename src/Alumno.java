import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Alumno extends Persona{
	
	private String matricula;

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public Alumno(String dNI, String nombre, String matricula) {
		super(dNI, nombre);
		this.matricula = matricula;
	}

	public Alumno() {
		super();
	}
	
	@Override
	public String toString() {
		return "Alumno [matricula=" + matricula + ", DNI=" + DNI + ", nombre=" + nombre + "]";
	}
	
}
