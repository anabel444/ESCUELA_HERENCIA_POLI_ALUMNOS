import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) {

		final int MOSTRAR = 1;
		final int CONTAR_PROFES_CATEGORIA = 2;
		final int BUSCAR_ALUMNO_NOMBRE = 3;
		final int SALIR = 0;

		ArrayList<Profesor> profesores = new ArrayList<Profesor>();
		ArrayList<Alumno> alumnos = new ArrayList<Alumno>();
		ArrayList<Conserje> conserjes = new ArrayList<Conserje>();

		load_data(profesores,alumnos,conserjes);

		
		int opcion = -1;
		Scanner sc = new Scanner(System.in);
		while (opcion != 0) {
			System.out.println("......................");
			System.out.println("\t\t\tESCUELA");
			System.out.println("......................");
			System.out.println("1-MostrarDatos");
			System.out.println("2-Contar Profesores por Categorias");
			System.out.println("3-Buscar alumno por  nombre");
			System.out.println("0-Salir");
			System.out.println("......................");
			System.out.print("Opci�n (1-3) 0.salir :  ");
			System.out.println("\n......................");

			try {
				opcion = Integer.parseInt(sc.nextLine());

				switch (opcion) {
				case MOSTRAR:
					
					for (Profesor aux:profesores){
						System.out.println(aux.toString()); 
					}
					for (Alumno aux:alumnos){
						System.out.println(aux.toString()); 
					}
					for (Conserje aux:conserjes){
						System.out.println(aux.toString()); 
					}
					break;
				case CONTAR_PROFES_CATEGORIA:

					break;
				case BUSCAR_ALUMNO_NOMBRE:

					break;
				case SALIR:

					break;
				default:
					break;
				} /*fin switch*/
				
			} catch (NumberFormatException e) {

				System.out.println("\tQUEEEEE NOOO  Elige una opcion (1-3) 0.salir");
			}
		} /*fin while*/

		
		
		
	} /* fin main */
	public static void load_data(ArrayList<Profesor> profesores,
			ArrayList<Alumno> alumnos,ArrayList<Conserje> conserjes)
	{		
		load_alumnos(alumnos);
		load_profesores(profesores);
		load_conserjes(conserjes);	
		
	}	
	public static void load_alumnos(ArrayList<Alumno> alumnos)
	{
		
		String line,DNI,nombre,matricula;
		String[] split_line;
		try {
		File fich = new File ("src/datos/Alumno.txt");
		Scanner sc = new Scanner (fich);
		while(sc.hasNextLine())
			{
				line = sc.nextLine();
				split_line= line.split("::");
				DNI=split_line[0];
				nombre=split_line[1];
				matricula=split_line[2];
				
				Alumno nuevo=new Alumno(DNI,nombre,matricula);
				
				alumnos.add(nuevo);
				}	
		sc.close();			 
		}catch(FileNotFoundException e){
		System.out.println("El archivo no existe...");
		}
	}
	
} /*fin class*/
